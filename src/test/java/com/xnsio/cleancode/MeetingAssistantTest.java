package com.xnsio.cleancode;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.util.*;

public class MeetingAssistantTest {
    private MeetingAssistant assistant = new MeetingAssistant();
    @Test
    public void scheduleMeetingWihtOnePersonForSameDay() throws NoFreeAvailableSlots {
        List<Person> personList = new ArrayList<>();
        personList.add(createPersonWith(LocalDate.now(), Arrays.asList(8,9,10,11,12,13,14,15)));
        Slot bookedSlot = new Slot(LocalDate.now(),16);
        Assert.assertEquals(bookedSlot,assistant.scheduleMeeting(personList,LocalDate.now(), 17));
    }

    @Test(expected = NoFreeAvailableSlots.class)
    public void noMeetingScheduledAfterWorkingHour() throws NoFreeAvailableSlots {
        List<Person> personList = new ArrayList<>();
        personList.add(createPersonWith(LocalDate.now(), Arrays.asList(9,11)));
        assistant.scheduleMeeting(personList,LocalDate.now(), 18);
    }


    @Test(expected = NoFreeAvailableSlots.class)
    public void noMeetingScheduledBeforeWorkingHour() throws NoFreeAvailableSlots {
        List<Person> personList = new ArrayList<>();
        personList.add(createPersonWith(LocalDate.now(), Arrays.asList(9,11)));
        assistant.scheduleMeeting(personList,LocalDate.now(), 8);
    }

    @Test
    public void scheduleMeetingWithMultiplePersons() throws NoFreeAvailableSlots {
        List<Person> personList = new ArrayList<>();
        personList.add(createPersonWith(LocalDate.now(),Arrays.asList(10,4)));
        personList.add(createPersonWith(LocalDate.now(), Arrays.asList(9,11)));
        Slot bookedSlot = new Slot(LocalDate.now(),8);
       Assert.assertEquals(bookedSlot,assistant.scheduleMeeting(personList,LocalDate.now(), 16));

    }

    @Test
    public void scheduleMeetingWithPersonBeforeADate() throws NoFreeAvailableSlots {
        List<Person> personList = new ArrayList<>();
        List<List<Integer>> bookedSlotOfPerson = new ArrayList<>();
        bookedSlotOfPerson.add(Arrays.asList(8,9,10,11,12));
        bookedSlotOfPerson.add(Arrays.asList(10,11,12,13));
        personList.add(createPersonWithMultipleDateInCalendar(bookedSlotOfPerson));
        Slot bookedSlot = new Slot(LocalDate.now(),13);
        Assert.assertEquals(bookedSlot,assistant.scheduleMeeting(personList,LocalDate.now(),14));
    }

    @Test(expected = NoFreeAvailableSlots.class)
    public void shouldNotScheduleMeetingWithPersonBeforeADate() throws NoFreeAvailableSlots {
        List<Person> personList = new ArrayList<>();
        List<List<Integer>> bookedSlotOfPerson = new ArrayList<>();
        bookedSlotOfPerson.add(Arrays.asList(8,9,10,11,12));
        bookedSlotOfPerson.add(Arrays.asList(8,9,10,11,12,13));
        personList.add(createPersonWithMultipleDateInCalendar(bookedSlotOfPerson));
        Slot bookedSlot = new Slot(LocalDate.now().plusDays(1),8);
        Assert.assertEquals(bookedSlot,assistant.scheduleMeeting(personList,LocalDate.now().plusDays(1),12));
    }

    private Person createPersonWithMultipleDateInCalendar(List<List<Integer>> bookedSlotOfPerson) {
        Map<LocalDate, List<Integer>> personCalendar= new HashMap<>();
        int i =0;
       for(List<Integer> bookedSlotsForADay:bookedSlotOfPerson){
           personCalendar.put(LocalDate.now().plusDays(i),bookedSlotsForADay);
           i++;
       }
        return new Person(personCalendar);
    }

    private Person createPersonWith(LocalDate date, List<Integer> bookedSlots) {
        Map<LocalDate, List<Integer>> personCalendar= new HashMap<>();
        personCalendar.put(date,bookedSlots);
        return  new Person(personCalendar);
    }

}
