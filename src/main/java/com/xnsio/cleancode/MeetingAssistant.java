package com.xnsio.cleancode;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class MeetingAssistant {

    private static final int WORKING_HOUR_TOP_THRESHOLD = 17;
    private static final int WOKRING_HOUR_BOTTUM_THRESHOLD = 8;
    private static final NoFreeAvailableSlots NO_FREE_SLOT_AVAILABLE = new NoFreeAvailableSlots("No Free Slot Available");
    private final List<Integer> workingHours = Arrays.asList(8,9,10,11,12,13,14,15,16);

    Slot scheduleMeeting(List<Person> personList, LocalDate beforeDate, int beforeTime) throws NoFreeAvailableSlots  {
        if(isRequestedTimeOutOfWorkingHours(beforeTime)) throw NO_FREE_SLOT_AVAILABLE;
        List<LocalDate> datesForMeeting = getPossibleDatesForMeeting(beforeDate);
        Slot slot = getSlotFrom(personList, beforeTime, datesForMeeting);
        if(slot.getTime() ==-1) throw NO_FREE_SLOT_AVAILABLE;
        return slot;
    }

    private Slot getSlotFrom(List<Person> personList, int beforeTime, List<LocalDate> datesToCheck) {
        return datesToCheck.stream()
                .map(date -> getSlotForDate(personList, date, beforeTime))
                .filter(possibleSlot -> possibleSlot.getTime() != -1).findFirst()
                .orElse(new Slot(LocalDate.now(), -1));
    }

    private boolean isRequestedTimeOutOfWorkingHours(int beforeTime) {
        return beforeTime > WORKING_HOUR_TOP_THRESHOLD || beforeTime  <= WOKRING_HOUR_BOTTUM_THRESHOLD;
    }

    private Slot getSlotForDate(List<Person> personList, LocalDate date, int beforeTime)  {
       List<Integer> commonFreeSlots = findCommonFreeSlotsFor(personList, date);
       commonFreeSlots.sort(Integer::compareTo);
       Integer availableSlot = findValidAvailableSlotFrom(commonFreeSlots, beforeTime);
       return new Slot(date,availableSlot);
    }

    private List<Integer> findCommonFreeSlotsFor(List<Person> personList, LocalDate date) {
        List<Integer> commonFreeSlots = new ArrayList<>(workingHours);
        personList.stream()
                .map(person -> person.findAvailableHours(workingHours, date))
                .forEach(commonFreeSlots::retainAll);
        return commonFreeSlots;
    }

    private Integer findValidAvailableSlotFrom(List<Integer> commons, int beforeTime) {
        return commons.stream()
                .filter(integer -> integer <= beforeTime - 1)
                .findFirst()
                .orElse(-1);
    }

    private List<LocalDate> getPossibleDatesForMeeting(LocalDate endDate) {
        List<LocalDate> dates = new ArrayList<>();
        LocalDate currentDay = LocalDate.now();
        while (currentDay.isBefore(endDate.plusDays(1))) {
            dates.add(currentDay);
           currentDay = currentDay.plusDays(1);
        }
        return dates;
    }
}
