package com.xnsio.cleancode;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Person {
    private Map<LocalDate, List<Integer>> calendar;
    Person(Map<LocalDate, List<Integer>> calendar) {
        this.calendar = calendar;
    }

    private Map<LocalDate, List<Integer>> getCalendarOfPerson() {
        return calendar;
    }

    List<Integer> findAvailableHours(List<Integer> workingHours, LocalDate date){
        return workingHours.stream()
                .filter(integer1 -> !this.getCalendarOfPerson().get(date).contains(integer1))
                .collect(Collectors.toList());
    }
}
