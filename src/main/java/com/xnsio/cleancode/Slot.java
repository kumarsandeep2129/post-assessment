package com.xnsio.cleancode;

import java.time.LocalDate;

public class Slot{
    private final LocalDate date;
    private final Integer time;

    Slot(LocalDate date, Integer time) {
        this.date = date;
        this.time = time;
    }

    public Integer getTime() {
        return time;
    }

    @Override
    public boolean equals(Object o) {
        Slot target = (Slot) o;
        return this.date.equals(target.date) && this.time.equals(target.time);
    }
}
