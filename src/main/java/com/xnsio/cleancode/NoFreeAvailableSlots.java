package com.xnsio.cleancode;

class NoFreeAvailableSlots extends Exception {
    NoFreeAvailableSlots(String s) {
        super(s);
    }
}
